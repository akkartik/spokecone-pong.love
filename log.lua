--
-- log.lua
-- https://github.com/rxi/log.lua
--
-- Copyright (c) 2016 rxi
--
-- This library is free software; you can redistribute it and/or modify it
-- under the terms of the MIT license.
--
-- Modifications: emit file+line number for a parameterized stack frame, so
-- that we can create logging helpers with this as a primitive.

local log = { _version = "0.1.0" }

log.outfile = nil
log.level = "trace"


local modes = {
  { name = "trace" },
  { name = "debug" },
  { name = "info" },
  { name = "warn" },
  { name = "error" },
  { name = "fatal" },
}


local levels = {}
for i, v in ipairs(modes) do
  levels[v.name] = i
end


local round = function(x, increment)
  increment = increment or 1
  x = x / increment
  return (x > 0 and math.floor(x + .5) or math.ceil(x - .5)) * increment
end


local _tostring = tostring

local tostring = function(...)
  local t = {}
  for i = 1, select('#', ...) do
    local x = select(i, ...)
    if type(x) == "number" then
      x = round(x, .01)
    end
    t[#t + 1] = _tostring(x)
  end
  return table.concat(t, " ")
end


for i, x in ipairs(modes) do
  local nameupper = x.name:upper()
  log[x.name..'_with_stack_frame'] = function(stack_frame_index, ...)
    
    -- Return early if we're below the log level
    if i < levels[log.level] then
      return
    end

    local msg = tostring(...)
    local info = debug.getinfo(stack_frame_index, "Sl")
    local lineinfo = info.short_src .. ":" .. info.currentline

    -- Output to console
    print(string.format("%s: %s", lineinfo, msg))

    -- Output to log file
    if log.outfile then
      local fp = io.open(log.outfile, "a")
      fp:write(string.format("%s: %s\n", lineinfo, msg))
      fp:close()
    end

  end
  log[x.name] = function(...)
    log[x.name..'_with_stack_frame'](3, ...)
  end
end


return log

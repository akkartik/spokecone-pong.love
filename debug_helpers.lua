json = require 'json'

function log_state()
	log.debug_with_stack_frame(3, json.encode({name='state', Ballp=Ballp, Ballv=Ballv, A=A, B=B}))
end

-- for section delimiters we'll use specific Unicode box characters
function log_start(name)
	log.debug_with_stack_frame(3, '\u{250c} ' .. name)
end
function log_end(name)
	log.debug_with_stack_frame(3, '\u{2518} ' .. name)
end

function log_new(name)
	log_end(name)
	log_start(name)
end

-- rendering graphical objects within sections/boxes
debug_log_render = {}

Ball_color = {r=0.5, g=0, b=0.5}
A_color = {r=0, g=0.5, b=0}
B_color = {r=0, g=0, b=0.5}

function debug_log_render.state(state, x,y, w)
	-- emit drawing to given space on screen (x to x+w, y downward); return lowest y drawn
	local h = 100
	love.graphics.rectangle('line', x+30,y, 100,h)
	local starty = y
	-- translate coordinate system
	x,y = x+30+50, y+50
	-- render -- with the actual numbers as Tufte commands
	App.color(Ball_color)
	love.graphics.circle('fill', x+state.Ballp.x, y+state.Ballp.y, 3)
	love.graphics.print(('%d,%d'):format(state.Ballp.x, state.Ballp.y), x+state.Ballp.x-40, y+state.Ballp.y-40)
	love.graphics.line(x+state.Ballp.x, y+state.Ballp.y, x+state.Ballp.x+state.Ballv.x*100, y+state.Ballp.y+state.Ballv.y*100)
	love.graphics.print(('Ball velocity: %0.2f,%0.2f'):format(state.Ballv.x, state.Ballv.y), x+200,starty+50)
--? 	love.graphics.print(('%0.2f,%0.2f -> %0.2f,%0.2f'):format(x+state.Ballp.x, y+state.Ballp.y, x+state.Ballp.x+state.Ballv.x, y+state.Ballp.y+state.Ballv.y), x+200, starty+50)  -- heh, debug prints for my debug prints
	App.color(A_color)
	love.graphics.rectangle('fill', x-50, y-state.A.ymin, 3, state.A.ymax-state.A.ymin)
	App.color(B_color)
	love.graphics.rectangle('fill', x+50-3+1, y-state.B.ymin, 3, state.B.ymax-state.B.ymin)
	App.color(Text_color)
	--
	return h+20
end
